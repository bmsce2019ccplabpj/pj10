#include<stdio.h> 
#include<math.h> 
int main()
{ 
int a,b,c;
float d,x1,x2;
printf("Enter the values of a,b,c\n");
scanf("%d%d%d",&a,&b,&c);
printf("The given equation is:\n%dx^2+%dx+%d=0",a,b,c);
d=pow(b,2)-4*a*c;
x1=(-b+sqrt(d))/(float)2*a;
x2=(-b-sqrt(d))/(float)2*a;
if(d<0)
printf("The roots are imaginary");
if(d==0)
printf("The roots are real and distinct\n roots are %f and %f",x1,x2);
if(d>0)
printf("The roots are real\nroots are %f and %f",x1,x2);
return 0;
 }