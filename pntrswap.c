#include<stdio.h>
void input(int *,int *);
void swap(int *,int *);
void output(int *,int *);
int main()
{
int a,b;
input(&a,&b);
swap(&a,&b);
output(&a,&b);
return 0;
}
void input(int *a,int *b)
{
printf("Enter the first number:\n");
scanf("%d",&*a);
printf("Enter the second number:\n");
scanf("%d",&*b);
}
void swap(int *a,int *b)
{
int temp=*a;
*a=*b;
*b=temp;
}
void output(int *a,int *b)
{
printf("The Swapped values are %d & %d",*a,*b);
}
